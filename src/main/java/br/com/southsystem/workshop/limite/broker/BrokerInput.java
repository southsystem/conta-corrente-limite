package br.com.southsystem.workshop.limite.broker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BrokerInput {

    String CONTA_CORRENTE_CRIADA = "contaCorrenteCriada";

    @Input(BrokerInput.CONTA_CORRENTE_CRIADA)
    SubscribableChannel contaCorrenteCriada();

}
