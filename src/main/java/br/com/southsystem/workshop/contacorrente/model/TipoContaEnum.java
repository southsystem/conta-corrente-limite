package br.com.southsystem.workshop.contacorrente.model;

public enum TipoContaEnum {
    PF, POUPANCA, PJ;
}
